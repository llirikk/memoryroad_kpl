$(document).ready(function () {

    let MemoryRoad = {

        step: 1,
        errorText: 'По Вашему запросу ничего не найдено',
        searchStatus: true,
        searchUrl: "http://78.107.253.74:88/api/",
        searchParams: {
            query: '',
            location: '',
        },
        broker: {
            el: '',
            page: 1,
            name: '',
            infinityScroll: false,
            sub: false,
            disabledChild: false,
            history: []
        },
        history: {},
        state: {
            location: [],
            burials: [],
            persons: [],
            total: [],
            parentName: '',
        },

        render() {
            $('#app .step').hide();
            $('#app .step-' + MemoryRoad.step).show();
        },
        stepZero() {
            this.render();
            $("#resp-box").mCustomScrollbar({
                callbacks: {
                    onTotalScroll: function () {
                        $('#resp-box').css('opacity', '0.2');
                        MemoryRoad.infinityScrollFull();
                    }
                }
            });

            $(".step-3 .layer").mCustomScrollbar({
                callbacks: {
                    onTotalScroll: function () {
                        $(".step-3 .layer .mCSB_container").css('opacity', '0.2');
                        MemoryRoad.infinityScrollPeople();
                    }
                }
            });
            this.checkSearchField();
        },
        // stepUp() {
        //     this.step++;
        //     if (this.step > 3) {
        //         this.step = 3;
        //     }
        //     this.render();
        // },
        clear() {
            $("#resp-box .mCSB_container").empty();
            $(".step-3 .layer .mCSB_container").empty();
        },
        checkSearchField() {
            $("#input-search").on('keyup', function (e) {
                MemoryRoad.searchParams.query = $(this).val();
                if (MemoryRoad.searchParams.query.length > 2) {
                    if (MemoryRoad.searchStatus) {
                        MemoryRoad.searchStatus = false;
                        MemoryRoad.broker.page = 1;
                        MemoryRoad.stepOne();
                    }
                }
            });
        },

        stepOne() {
            
            MemoryRoad.clear();
            
            $.ajax({
                method: "GET",
                dataType: "json",
                url: this.searchUrl + 'search' + '?' + 'query=' + this.searchParams.query + '&' + 'page=' + this.broker.page,
                success(resp) {
                    
                    if ((resp.data.location.length === 0) && (resp.data.burials.length === 0) && (resp.data.persons.length === 0)) {
                        $("#resp-box .mCSB_container").html(`<div class="text-error">${MemoryRoad.errorText}<div>`);
                    } else {
                        MemoryRoad.state.location = resp.data.location.slice(0, 15);
                        for (i = 0; i < MemoryRoad.state.location.length; i++) {
                            $("#resp-box .mCSB_container").append(`<div class='item-location' data-id=l${MemoryRoad.state.location[i].id} data-childs=${MemoryRoad.state.location[i].child_count}> ${MemoryRoad.state.location[i].name} </div>`);
                        }
                        $('.item-location:last-child').css('border-bottom', '1px solid rgba(255,255,255,.3)');

                        MemoryRoad.state.burials = resp.data.burials.slice(0, 15);
                        for (i = 0; i < MemoryRoad.state.burials.length; i++) {
                            let place = MemoryRoad.state.burials[i].place.split(' ').join('_');
                            $("#resp-box .mCSB_container").append(`<div class='item-place' data-id='b${place}' data-childs=${MemoryRoad.state.burials[i].child_count}> ${MemoryRoad.state.burials[i].place}, ${MemoryRoad.state.burials[i].name.split('\n')[0]} </div>`);
                        }
                        $('.item-place:last-child').css('border-bottom', '1px solid rgba(255,255,255,.3)');

                        MemoryRoad.state.persons = resp.data.persons;
                        for (i = 0; i < MemoryRoad.state.persons.length; i++) {
                            let name = MemoryRoad.state.persons[i].name.split(' ').join('_');
                            $("#resp-box .mCSB_container").append(`<div data-id='p${name}'>${MemoryRoad.state.persons[i].name}</div>`);
                        }
                    }

                    $('#resp-box').addClass('show');
                    MemoryRoad.searchStatus = true;

                    $(document).on('click', '#resp-box .mCSB_container div', function (event) {

                        MemoryRoad.broker.el = $(this).data('id');
                        MemoryRoad.broker.name = $(event.target).text();

                        if (MemoryRoad.broker.el.indexOf('l') === 0) {
                            MemoryRoad.broker.el = MemoryRoad.broker.el.slice(1);
                            $.ajax({
                                method: "GET",
                                dataType: "json",
                                url: `${MemoryRoad.searchUrl}location/${MemoryRoad.broker.el}`,
                                success(resp) {
                                    if (resp.data.items == 0) {
                                        MemoryRoad.state.total = resp.data.burials;
                                    } else {
                                        MemoryRoad.state.total = resp.data.items;
                                    }
                                    MemoryRoad.stepTwo();
                                }
                            })
                        }
                        if (MemoryRoad.broker.el.indexOf('b') === 0) {
                            MemoryRoad.broker.el = MemoryRoad.broker.el.slice(1).split('_').join('%20');
                            $.ajax({
                                method: "GET",
                                dataType: "json",
                                url: `${MemoryRoad.searchUrl}burials?place=${MemoryRoad.broker.el}`,
                                data: '',
                                success(resp) {
                                    MemoryRoad.state.total = resp.data.items;
                                    MemoryRoad.stepTwo();
                                }
                            })
                        }
                        if (MemoryRoad.broker.el.indexOf('p') === 0) {

                            MemoryRoad.broker.el = MemoryRoad.broker.el.slice(1).split('_').join('%20');
                            $.ajax({
                                method: "GET",
                                dataType: "json",
                                url: `${MemoryRoad.searchUrl}burials?person=${MemoryRoad.broker.el}`,
                                data: '',
                                success(resp) {
                                    console.log('this', resp);
                                    MemoryRoad.state.total = resp.data.items;
                                    MemoryRoad.broker.disabledChild = true;
                                    MemoryRoad.stepTwo();
                                }
                            })
                        }

                    });

                }

            });

        },
        infinityScrollFull() {

            MemoryRoad.broker.page++;

            $.ajax({
                method: "GET",
                dataType: "json",
                url: this.searchUrl + 'search' + '?' + 'query=' + this.searchParams.query + '&' + 'page=' + this.broker.page,
                success(resp) {
                    MemoryRoad.state.persons = resp.data.persons;
                    for (i = 0; i < MemoryRoad.state.persons.length; i++) {
                        let name = MemoryRoad.state.persons[i].name.split(' ').join('_');
                        $("#resp-box .mCSB_container").append(`<div data-id='p${name}'> ${MemoryRoad.state.persons[i].name} </div>`);
                    }
                    MemoryRoad.searchStatus = true;
                    $('#resp-box').css('opacity', '1');
                }

            });

        },

        stepTwo() {

            MemoryRoad.step = 2;
            MemoryRoad.render();
            function renderTwo() {
                
                // console.log('MemoryRoad.state.parentName:', MemoryRoad.state.parentName)
                    // console.log('resp.data.location.name:', resp.data.location.name)

                    // <div>
                    //     <div> ${MemoryRoad.state.total[i].parent} </div>
                    // </div>
                let nameArray = []
                for (i = 0; i < MemoryRoad.state.total.length; i++) {
                    // console.log('step2 - MemoryRoad.state.total[i].name', MemoryRoad.state.total[i].name)
                    MemoryRoad.broker.sub = MemoryRoad.state.total[i].child_count > 0 ? true : false;
                    let information = MemoryRoad.state.total[i].child_count > 0 ? `<div> ИНФОРМАЦИЯ- ${MemoryRoad.state.total[i].child_count} шт. </div>`: ''
                    
                    $('.step.step-2 ul')
                        .append(`
                        <li style='color:white;' class='get-burial' data-sub='${MemoryRoad.broker.sub}' data-id='${MemoryRoad.state.total[i].id}'>
                            <div> ${MemoryRoad.state.total[i].code == undefined ? "" : MemoryRoad.state.total[i].code} </div>
                            <div> ${MemoryRoad.broker.name} </div>
                            <div class='findName'> ${MemoryRoad.state.total[i].name.split('\n').map(elem => `<p>${elem}</p>`).join('')} </div>
                            ${information}
                        </li>`);
                        // nameArray.push(MemoryRoad.state.total[i].name)
                    }
                    // console.log('nameArray', nameArray )
                $('.heroesList__top').html(`<h1 class='section__title heroesList__title'>${MemoryRoad.broker.name}</h1>`);
                $('.burials_count').html(`<h2 class='heroesList__title'>${MemoryRoad.state.total.length} шт.</h2>`);
            }
            renderTwo();
            // console.log('this', $('.findName').html())
            // $(document).on('click', $(this), function () {
                // console.log('object', $(this).html())
                
                // console.log('третий div с p', $('.findName div:nth-child(2) > p').html())
                
                // MemoryRoad.broker.name = $(this).html()
            // })

                MemoryRoad.broker.el = $(this).data('id');
                // MemoryRoad.broker.name = $(event.target).text();

            $(document).on('click', '.get-burial', function () {
                // console.log('get-burial', $(this).get(0).innerHTML)
                // let result = $(this).get(0).innerHTML
                // let paragraphResult = result.split('</div>/n')
                // console.log('paragraphResult', paragraphResult)
                

                MemoryRoad.broker.sub = $(this).data('sub');
                // console.log('MemoryRoad.broker.sub && !(MemoryRoad.broker.disabledChild)',MemoryRoad.broker.sub ,!(MemoryRoad.broker.disabledChild))
                
                if ($(this).data('id') !== undefined) {
                    MemoryRoad.broker.el = $(this).data('id');
                }
                
                if ($(this).data('page') !== undefined) {
                    MemoryRoad.broker.page = $(this).data('page');
                    // MemoryRoad.broker.name = $(this).html();
                }

                let url = '';
                if (MemoryRoad.broker.sub && !(MemoryRoad.broker.disabledChild)){
                    url = MemoryRoad.searchUrl + "location/" + MemoryRoad.broker.el;
                } else {
                    url = MemoryRoad.searchUrl + "burial/" + MemoryRoad.broker.el + "?page=" + MemoryRoad.broker.page;
                }

                $.ajax({
                    method: "GET",
                    dataType: "json",
                    url: url,
                    success(resp) {
                        MemoryRoad.state.parentName = resp.data.name;
                        // console.log('MemoryRoad.state:', MemoryRoad.state)
                        if(MemoryRoad.broker.sub && !(MemoryRoad.broker.disabledChild)){
                            MemoryRoad.state.total = resp.data.items.length > 0 ? resp.data.items : resp.data.burials;
                            $('.step.step-2 ul').empty();
                            renderTwo();
                        } else {
                            // console.log('places', resp);
                            MemoryRoad.state.total = resp.data;
                            MemoryRoad.step = 3;
                            MemoryRoad.render();
                            MemoryRoad.stepThree();
                        }
                        // console.log('MemoryRoad.state.total:', MemoryRoad.state.total)
                    }
                })
            });

        },

        stepThree() {

            let block = $(".postament__grid > div > div");
            let sleeveNumber = (MemoryRoad.state.total.row - 1) * 12 + (MemoryRoad.state.total.col);
            let widthBlock = block.width();
            block.css('height', widthBlock / 3);

            $('.postament__grid > div').eq(MemoryRoad.state.total.row - 1).find('div').eq(MemoryRoad.state.total.col - 1).css('background-color', '#CBA77D');

            $('.battle_postament-grid-coordination').html(MemoryRoad.state.total.code);
            $('.burialPlace').html(MemoryRoad.state.total.name.split('\n').map(elem => `<p>${elem}</p>`));
            $('.coordinations').html("Столбец " + MemoryRoad.state.total.col + ", строка " + MemoryRoad.state.total.row);
            $('.battle_postament-number').html("Постамент №" + MemoryRoad.state.total.postament.id);
            $('.battle__name').html(MemoryRoad.state.total.postament.name);
            $('.battle__date').html(`(${MemoryRoad.state.total.postament.date})`);

            console.log('MemoryRoad.state.total.persons', MemoryRoad.state.total.persons);

            if(MemoryRoad.state.total.persons.length > 0){
                for (i = 0; i < MemoryRoad.state.total.persons.length; i++) {
                    let nameActive = 'not';
                    if(MemoryRoad.state.total.persons[i].name === MemoryRoad.broker.name) {
                        nameActive = 'active'
                    }
                    $(".step-3 .layer .mCSB_container").append(`
                        <div class='item-person ${nameActive}'>
                            <div class='person-info person-info-rank'>${MemoryRoad.state.total.persons[i].rank}</div>
                            <div class='person-info'>${MemoryRoad.state.total.persons[i].name}</div>
                        </div>
                    `);
                }
            } else {
                $(".step-3 .layer").hide();
                $(".step-3 .row-list").html("<p class='text-empty'>Информация не найдена</p>");
            }

            $('#Layer_4 > path').hide();
            $('#Layer_4 > path').css('stroke-width', '3px').css('stroke-dasharray', '5, 5');
            $('#Layer_4 > path:nth-child(' + MemoryRoad.state.total.postament.id + ')').show();
            $('#Layer_3 > g:nth-child(' + MemoryRoad.state.total.postament.id + ') > polygon').css('stroke-width', '10').css('stroke', '#cca87d');
            $(`#Group_7 > path:nth-child(${sleeveNumber})`).css('fill', '#cca87d');

        },
        infinityScrollPeople() {

            MemoryRoad.broker.page++;

            $.ajax({
                method: "GET",
                dataType: "json",
                url: MemoryRoad.searchUrl + "burial/" + MemoryRoad.broker.el + "?page=" + MemoryRoad.broker.page,
                data: '',
                success(resp) {

                    for (i = 0; i < resp.data.persons.length; i++) {
                        let nameActive = resp.data.persons[i].name === MemoryRoad.broker.name ? 'active' : '';
                        let rank = resp.data.persons[i].rank === '' ?  'неизвестен' : resp.data.persons[i].rank;
                        $(".step-3 .layer .mCSB_container").append(`<div class='go-to-step-2 ${nameActive}'><div class='person-info'>${rank}</div><div class='person-info'>${resp.data.persons[i].name}</div></div>`);
                    }

                    $(".step-3 .layer .mCSB_container").css('opacity', '1');

                }
            })
        }

    };

    MemoryRoad.stepZero();

});